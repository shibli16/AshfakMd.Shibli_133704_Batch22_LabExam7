<?php
namespace App\SEIP133704\File;
use App\SEIP133704\File\Message;
use App\SEIP133704\File\Utility;

class File
{
    public $id = "";
    public $name = "";
    public $filename = "";
    
    public $conn;
    
    public  function __construct()
    {
        $this->conn = mysqli_connect("localhost","root","","labxm7b22") or die("Database connection failed");
                
    }
    public function prepare($data = array())
    {
        if(array_key_exists("id",$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists("name",$data)){
            $this->name = $data['name'];
        }
        if(array_key_exists("file",$data)){
            $this->filename = $data['file'];
        }
    }
    public function store()
    {
        $query = "INSERT INTO `labxm7b22`.`file` ( `name`, `filename`) VALUES ( '".$this->name."', '".$this->filename."')";
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data Inserted Successfully");
            Utility::redirect("index.php");
        }
        else {
            Message::message("Data Not Inserted");
            Utility::redirect("index.php");
        }
    }

    public function index()
    {
        $_list = array();
        $query = "SELECT * FROM `file` WHERE `deleted_at` is NULL";
        $result = mysqli_query($this->conn,$query);
        if($result){
            while ($row = mysqli_fetch_object($result)){
                $_list[] = $row;
            }
        }
        return $_list;
    }

    public function active()
    {
        $query = "UPDATE `labxm7b22`.`file` SET `active` = NULL ";
        $result = mysqli_query($this->conn,$query);
        $active = time();
        $query = "UPDATE `labxm7b22`.`file` SET `active` = '".$active."' WHERE `file`.`id` = ".$this->id;
        
        $result = mysqli_query($this->conn,$query);
        if($result){
            Utility::redirect("index.php");
        }
    }
    public function deactive()
    {

        $query = "UPDATE `labxm7b22`.`file` SET `active` = NULL WHERE `file`.`id` = ".$this->id;

        $result = mysqli_query($this->conn,$query);
        if($result){
            Utility::redirect("index.php");
        }
    }

    public function getActive()
    {
        $query = "SELECT * FROM `file` WHERE `active` is NOT NULL";
        $result = mysqli_query($this->conn,$query);

        $row = mysqli_fetch_object($result);
        return $row;
    }
    public function view()
    {
        $row = "";
        $query = "SELECT * FROM `file` WHERE `id`=".$this->id;

        $result = mysqli_query($this->conn,$query);
        if($result){
            $row = mysqli_fetch_object($result);
        }
        return $row;
    }
    public function update()
    {
        if(empty($this->filename)) {
            $query ="UPDATE `labxm7b22`.`file` SET `name` = '".$this->name."' WHERE `file`.`id` =".$this->id;
        }
        else {
            $query = "UPDATE `labxm7b22`.`file` SET `name` = '".$this->name."' ,`filename` = '".$this->filename."' WHERE `file`.`id` =".$this->id;
        }
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data Updated Successfully");
            Utility::redirect("index.php");
        }
        else {
            Message::message("Data not Updated");
            Utility::redirect("index.php");
        }
    }

    public function delete()
    {
        $query = "DELETE FROM `labxm7b22`.`file` WHERE `file`.`id` =".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data Deleted Successfully");
            Utility::redirect("index.php");
        }
        else {
            Message::message("Data Not Deleted");
            Utility::redirect("index.php");
        }

    }


}