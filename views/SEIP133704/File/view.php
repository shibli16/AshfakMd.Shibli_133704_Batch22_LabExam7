<?php
include_once ('../../../vendor/autoload.php');

use App\SEIP133704\File\File;
use App\SEIP133704\File\Message;
use App\SEIP133704\File\Utility;


$newView = new File();
$newView->prepare($_GET);
$item = $newView->view();


?>
<!DOCTYPE html>
<html lang="eng">
<head>
    <meta charset="UTF-8" lang="eng">
    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.6/css/bootstrap.min.css"/>
    <script src="../../../resources/bootstrap-3.3.6/js/bootstrap.min.js"></script>
    <script src="../../../resources/jquery-3.0.0.min.js" ></script>

    <title>File View</title>
</head>
<body>
<div class="container">
    <h2>File View</h2>
    <a href="index.php"> <button class="btn btn-success">Home</button></a>
    <a href="create.php"> <button class="btn btn-success">Add</button></a>
    <div class="container panel">
        <div class="panel-heading">
            <h3>ID: <?php echo  $item->id ?></h3>
            <h3>Name: <?php echo  $item->name ?></h3>
        </div>
        <div class="panel-body">
            File:
            <img src="../../../resources/files/<?php echo $item->filename ?>" height="300px" width="300px" >

        </div>
        
        
    </div>
</div>
</body>
</html>
