<?php
include_once ('../../../vendor/autoload.php');

use App\SEIP133704\File\File;
use App\SEIP133704\File\Message;
use App\SEIP133704\File\Utility;


$newDeactive = new File();
$newDeactive->prepare($_GET);
$newDeactive->deactive();