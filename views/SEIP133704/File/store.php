<?php
include_once ('../../../vendor/autoload.php');

use App\SEIP133704\File\File;
use App\SEIP133704\File\Message;
use App\SEIP133704\File\Utility;


if(isset($_FILES['file']) && !empty($_FILES['file']['name'])){
    $fileName = time().$_FILES['file']['name'];
    $tempLocation = $_FILES['file']['tmp_name'];

    move_uploaded_file($tempLocation,"../../../resources/files/".$fileName);
    $_POST['file'] = $fileName;
}


$newStore = new File();
$newStore->prepare($_POST);
$newStore->store();