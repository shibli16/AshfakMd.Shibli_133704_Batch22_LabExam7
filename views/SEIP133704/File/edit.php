<?php
include_once ('../../../vendor/autoload.php');

use App\SEIP133704\File\File;
use App\SEIP133704\File\Message;
use App\SEIP133704\File\Utility;

$edit = new File();
$edit->prepare($_GET);
$item = $edit->view();


?>
<!DOCTYPE html>
<html lang="eng">
    <head>
        <meta charset="UTF-8" lang="eng">
        <link rel="stylesheet" href="../../../resources/bootstrap-3.3.6/css/bootstrap.min.css"/>
        <script src="../../../resources/bootstrap-3.3.6/js/bootstrap.min.js"></script>
        <script src="../../../resources/jquery-3.0.0.min.js" ></script>

        <title>File Update</title>
    </head>
    <body>
        <div class="container">
            <h2>File Update</h2>
            <a href="index.php"> <button class="btn btn-success">Home</button></a>
            <a href="create.php"> <button class="btn btn-success">Add</button></a>
            <div class="container-fluid">
                <form class="form-horizontal form-group" method="post" action="update.php" enctype="multipart/form-data">
                    <div class="container col-sm-6">
                        <input type="hidden" name="id" value="<?php echo $item->id ?>">
                        <label for="name">Name: </label>
                        <input class="form-control" type="text" placeholder="Enter Your Name" name="name" value="<?php echo $item->name ?>">
                        <label for="file" >Upload File </label>
                        <input class="form-control" type="file" name="file">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>
