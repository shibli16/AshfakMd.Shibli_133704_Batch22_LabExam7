<?php
include_once ('../../../vendor/autoload.php');

use App\SEIP133704\File\File;
use App\SEIP133704\File\Message;
use App\SEIP133704\File\Utility;

$newView = new File();
$newView->prepare($_POST);
$item = $newView->view();
if(isset($_FILES['file']) && !empty($_FILES['file']['name'])){
    $fileName = time().$_FILES['file']['name'];
    $tempLocation = $_FILES['file']['tmp_name'];

    unlink($_SERVER['DOCUMENT_ROOT'].'/AshfakMd.Shibli_133704_Batch22_LabExam7/resources/files/'.$item->filename);
    move_uploaded_file($tempLocation,"../../../resources/files/".$fileName);
    $_POST['file'] = $fileName;
}


$newUpdate = new File();
$newUpdate->prepare($_POST);
$newUpdate->update();