<?php
session_start();
include_once ('../../../vendor/autoload.php');

use App\SEIP133704\File\File;
use App\SEIP133704\File\Message;
use App\SEIP133704\File\Utility;

$newIndex = new File();
$list = $newIndex->index();
$activeItem = $newIndex->getActive();


?>
<!DOCTYPE html>
<html lang="eng">
<head>
    <meta charset="UTF-8" lang="eng">
    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.6/css/bootstrap.min.css"/>
    <script src="../../../resources/bootstrap-3.3.6/js/bootstrap.min.js"></script>
    <script src="../../../resources/jquery-3.0.0.min.js" ></script>

    <title>File Upload</title>
</head>
<body>
<div class="container">
    <h2>File List</h2>
    <a href="index.php"> <button class="btn btn-success">Home</button></a>
    <a href="create.php"> <button class="btn btn-success">Add</button></a>
    <h3><?php echo Message::message(); ?></h3>
    <div class="panel panel-info">
        <div class="panel-heading">
            <h5>Profile Picture</h5>
        </div>
        <div class="panel-body">
            <?php
               if (is_null($activeItem)) echo ('<h4>There is no Active Profile Picture</h4>');
            else{
            ?>
            <img src="../../../resources/files/<?php echo $activeItem->filename ?>" height="200px" width="200px">


        <?php } ?>
        </div>

    </div>
    <div class="container">
        <table class="table table-bordered table-responsive">
            <tr>
                <th>SL</th>
                <th>ID</th>
                <th>Name</th>
                <th>Thumbnail</th>
                <th>Action</th>
            </tr>
            <?php
            $sl = 0;
            foreach ($list as $item){
                $sl++;

            ?>
            <tr>
                <td><?php echo $sl ?></td>
                <td><?php echo $item->id ?></td>
                <td><?php echo $item->name ?></td>
                <td><img src="../../../resources/files/<?php echo $item->filename ?>" height="50px" width="50px" ></td>
                <td>
                    <?php

                    if(isset($activeItem) && ($activeItem->id == $item->id) ) {?>
                    <a href="deactive.php?id=<?php echo $item->id ?>"> <button class="btn btn-danger">Make Deactive</button></a>
                    <?php } else { ?>
                    <a href="active.php?id=<?php echo $item->id ?>"> <button class="btn btn-success">Make Active</button></a>
                    <?php } ?>
                    <a href="view.php?id=<?php echo $item->id ?>"> <button class="btn btn-success">View</button></a>
                    <a href="edit.php?id=<?php echo $item->id ?>"> <button class="btn btn-success">Edit</button></a>
                    <a href="delete.php?id=<?php echo $item->id ?>"> <button class="btn btn-danger" onclick="return confirm('Are you sure want to delete?')">Delete</button></a>

                </td>
            </tr>
            <?php } ?>

        </table>

    </div>
</div>
</body>
</html>
