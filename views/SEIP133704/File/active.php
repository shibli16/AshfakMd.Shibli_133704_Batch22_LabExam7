<?php
include_once ('../../../vendor/autoload.php');

use App\SEIP133704\File\File;
use App\SEIP133704\File\Message;
use App\SEIP133704\File\Utility;


$newActive = new File();
$newActive->prepare($_GET);
$newActive->active();