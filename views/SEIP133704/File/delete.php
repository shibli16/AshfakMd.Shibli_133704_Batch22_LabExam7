<?php
include_once ('../../../vendor/autoload.php');

use App\SEIP133704\File\File;
use App\SEIP133704\File\Message;
use App\SEIP133704\File\Utility;

$newDelete = new File();
$newDelete->prepare($_GET);
$item = $newDelete->view();

unlink($_SERVER['DOCUMENT_ROOT'].'/AshfakMd.Shibli_133704_Batch22_LabExam7/resources/files/'.$item->filename);

$newDelete->delete();